---
date: "2016-11-08T16:00:00+02:00"
title: "オープンソース"
weight: 10
toc: false
draft: false
---
<h3 class="subtitle is-3">
	<svg class="octicon octicon-code" viewBox="0 0 14 16" version="1.1" aria-hidden="true">
		<path fill-rule="evenodd" d="M9.5 3L8 4.5 11.5 8 8 11.5 9.5 13 14 8 9.5 3zm-5 0L0 8l4.5 5L6 11.5 2.5 8 6 4.5 4.5 3z"></path>
	</svg>
	オープンソース
</h3>

コードは [code.gitea.io/gitea](https://code.gitea.io/gitea/) で入手できます！ 私たちと一緒に[コントリビューション](https://github.com/go-gitea/gitea)を行い、このプロジェクトをさらに優れたものにしていきましょう。コントリビュータになるのに遠慮はいりません！
